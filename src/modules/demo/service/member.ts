import { Init, Provide } from '@midwayjs/decorator';
import { BaseService } from '@cool-midway/core';
import { InjectEntityModel } from '@midwayjs/typeorm';
import { Repository } from 'typeorm';
import { DemoProjectMemberEntity } from '../entity/member';

/**
 * demo模块-项目成员关联
 */
@Provide()
export class DemoProjectMemberService extends BaseService {
  @InjectEntityModel(DemoProjectMemberEntity)
  demoProjectMemberEntity: Repository<DemoProjectMemberEntity>;

  @Init()
  async init() {
    await super.init();
    this.setEntity(this.demoProjectMemberEntity);
  }
}
