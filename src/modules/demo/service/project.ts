import { Init, Provide } from '@midwayjs/decorator';
import { BaseService } from '@cool-midway/core';
import { InjectEntityModel } from '@midwayjs/typeorm';
import { Repository } from 'typeorm';
import { ProjectInfoEntity } from '../entity/project';

/**
 * project模块-项目信息
 */
@Provide()
export class ProjectInfoService extends BaseService {
  @InjectEntityModel(ProjectInfoEntity)
  projectInfoEntity: Repository<ProjectInfoEntity>;

  @Init()
  async init() {
    await super.init();
    this.setEntity(this.projectInfoEntity);
  }
}
