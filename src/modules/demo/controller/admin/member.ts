import { BaseController, CoolController } from '@cool-midway/core';
import { Inject } from '@midwayjs/core';
import { DemoProjectMemberEntity } from '../../entity/member';
import { DemoProjectMemberService } from '../../service/member';

/**
 * demo模块-项目成员关联
 */
@CoolController({
  api: ['add', 'delete', 'update', 'info', 'list', 'page'],
  entity: DemoProjectMemberEntity,
  service: DemoProjectMemberService,
  pageQueryOp: {
    keyWordLikeFields: ['projectId', 'memberId'],
    fieldEq: ['projectId', 'memberId'],
  },
})
export class AdminDemoProjectMemberController extends BaseController {
  @Inject()
  demoProjectMemberService: DemoProjectMemberService;
}
