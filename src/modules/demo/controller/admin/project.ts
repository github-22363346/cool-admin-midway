import { CoolController, BaseController } from '@cool-midway/core';
import { Body, Inject, Post, Get, Query } from '@midwayjs/core';
import { ProjectInfoEntity } from '../../entity/project';
import { ProjectInfoService } from '../../service/project';

/**
 * project模块-项目信息
 */
@CoolController({
  api: ['add', 'delete', 'update', 'info', 'list', 'page'],
  entity: ProjectInfoEntity,
  service: ProjectInfoService,
  pageQueryOp: {
    keyWordLikeFields: ['name'],
    fieldEq: ['status', 'priority'],
  },
})
export class AdminProjectInfoController extends BaseController {
  @Inject()
  projectInfoService: ProjectInfoService;
}
