import { BaseEntity } from '@cool-midway/core';
import { Column, Entity, Index } from 'typeorm';

/**
 * demo模块-项目成员关联
 */
@Entity('demo_project_member')
export class DemoProjectMemberEntity extends BaseEntity {
  @Index()
  @Column({ comment: '项目id' })
  projectId: number;

  @Index()
  @Column({ comment: '成员id' })
  memberId: number;
}
