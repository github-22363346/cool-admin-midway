import { BaseEntity } from '@cool-midway/core';
import { Column, Entity, Index } from 'typeorm';

/**
 * project模块-项目信息
 */
@Entity('project_info')
export class ProjectInfoEntity extends BaseEntity {
  @Index()
  @Column({ comment: '名称' })
  name: string;

  @Column({ comment: '描述', type: 'text', nullable: true })
  description: string;

  @Index()
  @Column({ comment: '负责人' })
  leaderId: number;

  @Column({ comment: '状态 0-未开始 1-进行中 2-已完成', default: 0 })
  status: number;

  @Column({ comment: '截止日期', type: 'date' })
  deadline: Date;

  @Column({ comment: '优先级 0-低 1-中 2-高', default: 0 })
  priority: number;

  // @Column({ comment: '标签', nullable: true, type: 'json' })
  // labels: string[];

  // @Column({ comment: '关联任务', nullable: true, type: 'json' })
  // relatedTasks: string[];

  @Column({
    comment: '进度',
    type: 'decimal',
  })
  progress: number;

  @Column({ comment: '备注', nullable: true })
  remark: string;
}
